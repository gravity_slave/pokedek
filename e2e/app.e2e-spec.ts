import { PokedekPage } from './app.po';

describe('pokedek App', () => {
  let page: PokedekPage;

  beforeEach(() => {
    page = new PokedekPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
