import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../shared/pokemon';
import { PokemonService } from '../shared/pokemon.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-poke-add',
  templateUrl: './poke-add.component.html',
  styleUrls: ['./poke-add.component.css']
})
export class PokeAddComponent implements OnInit {
  cardTitle = 'Add Pokemon';
  errorMessage: string;
  formPokemon: any = {};
  mouseoverLogin = false;

  constructor(
    private pokemonService: PokemonService,
    private router: Router
  ) { }

  savePokemon(formValues: any) {
    this.pokemonService.addPokemon(formValues)
      .subscribe( res => {
        console.log('Pokemon Saved');
        this.router.navigate(['/']);
      },
        err => console.log('error', err)
        );
  }

  ngOnInit() {
  }

}
