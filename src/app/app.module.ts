import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing';
import { HttpModule } from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { PokemonData } from './shared/pokemon-data';
import { AppComponent } from './app.component';
import { PokeAddComponent } from './poke-add/poke-add.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { PokemonService } from './shared/pokemon.service';
import { PokeListComponent } from './poke-list/poke-list.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import './shared/rxjs-extensions';
@NgModule({
  declarations: [
    AppComponent,
    PokeAddComponent,
    NavComponent,
    HomeComponent,
    PokeListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(PokemonData),
    TooltipModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [ PokemonService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
