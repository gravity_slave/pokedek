import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Pokemon } from './pokemon';


@Injectable()
export class PokemonService {
  private pokeUrl = 'api/pokemons';

  constructor(private _http: Http) { }

  getPokemons(): Observable<Pokemon[]> {
    return this._http
      .get(this.pokeUrl)
      .map((res: Response) => <Pokemon[]> res.json().data)
      .do(data => console.log(data))
      .catch(this.handleError);
  }

  deletePokemon(pokemon: Pokemon): Observable<Response> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers});
    const url = `${this.pokeUrl}/${pokemon.id}`;
    return this._http.delete(url, options)
      .catch(this.handleError);
  }

  addPokemon(pokemon: any): Observable<Pokemon> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers});
    const body = JSON.stringify(pokemon);
    const url = `${this.pokeUrl}`;
    return this._http.post(url, body, options)
      .map( (res: Response) => {
        return res.json()
      })
      .do(data => console.log(data))
      .catch(this.handleError);
  }

  getPokemonDetails(id: number): Observable<Pokemon> {
    const url = `${this.pokeUrl}/${id}`;

    return this._http
      .get(url)
      .map((res: Response) => <Pokemon> res.json().data)
      .do(data => console.log(data))
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    const msg = `Error status code ${error.status} status ${error.statusText} at ${error.url}`;
    return Observable.throw(msg);
  }

}
