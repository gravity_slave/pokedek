import {Component, OnInit, ViewChild} from '@angular/core';
import { Pokemon } from '../shared/pokemon';
import { PokemonService } from '../shared/pokemon.service';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-poke-list',
  templateUrl: './poke-list.component.html',
  styleUrls: ['./poke-list.component.css']
})
export class PokeListComponent implements OnInit {
  errorMessage: string;
  pokemonList: Array<Pokemon>;
  @ViewChild('childModal') public childModal: ModalDirective;

  selectedPokemonLoaded = false;
  pokemonDetails: Pokemon;


  constructor(private pokemonService: PokemonService) { }

  ngOnInit() {
    this.getPokemons();
  }

  public hideChildModal() {
    this.childModal.hide();
  }

  getPokemons() {
  this.pokemonService.getPokemons()
    .subscribe( (pokemons: Array<Pokemon>)  => this.pokemonList = pokemons,
    errors => this.errorMessage = <any>errors);
 }

  viewSinglePokemon(id: number) {
    this.pokemonService.getPokemonDetails(id)
      .subscribe(
        (pokemon: Pokemon) => {
          this.pokemonDetails = pokemon;
          this.selectedPokemonLoaded = true;
          this.childModal.show();
        },
        error => this.errorMessage = <any> error
      )
  }

 deletePokemon(pokemon: Pokemon) {
    this.pokemonService.deletePokemon(pokemon)
      .subscribe(
        () => {},
        errors => this.errorMessage = <any>errors,
        () => this.getPokemons()
      );
 }

}
